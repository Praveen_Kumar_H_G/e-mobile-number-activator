package com.training.mobilenumberactivator.exception;

public class UserAlreadyActivatedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserAlreadyActivatedException(String message) {
		super(message);
	}

}
