package com.training.mobilenumberactivator.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationDto> handllerMethodAurgumentException(MethodArgumentNotValidException ex) {
		Map<String, String> errorMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors()
				.forEach(error -> errorMap.put(error.getField(), error.getDefaultMessage()));
		ValidationDto validationMessage = new ValidationDto();
		validationMessage.setValidationErrors(errorMap);
		validationMessage.setErrorCode("EX0007");
		validationMessage.setErrorMessage("please add valid input data");
		return new ResponseEntity<>(validationMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PlanNotFoundException.class)
	public ResponseEntity<ErrorResponse> planNotFoundExceptionHandler(PlanNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0001", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NumberNotFoundException.class)
	public ResponseEntity<ErrorResponse> numberNotFoundExceptionHandler(NumberNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0002", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RequestIdNotFoundException.class)
	public ResponseEntity<ErrorResponse> requestIdNotFoundExceptionHandler(RequestIdNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0003", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RequestAlreadyAcceptedException.class)
	public ResponseEntity<ErrorResponse> requestAlreadyAcceptedExceptionHandler(RequestAlreadyAcceptedException ex) {
		ErrorResponse error = new ErrorResponse("EX0004", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserAlreadyActivatedException.class)
	public ResponseEntity<ErrorResponse> userAlreadyActivatedExceptionHandler(UserAlreadyActivatedException ex) {
		ErrorResponse error = new ErrorResponse("EX0005", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErrorResponse> messageNotReadableHandler(HttpMessageNotReadableException ex) {
		ErrorResponse error = new ErrorResponse("EX0006", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
