package com.training.mobilenumberactivator.exception;

public class RequestAlreadyAcceptedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RequestAlreadyAcceptedException(String message) {
		super(message);
	}
}


