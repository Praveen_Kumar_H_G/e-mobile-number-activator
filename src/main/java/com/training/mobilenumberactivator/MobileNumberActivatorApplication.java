package com.training.mobilenumberactivator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileNumberActivatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileNumberActivatorApplication.class, args);
	}

}
